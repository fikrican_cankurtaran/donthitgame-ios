//
//  LandingPageViewController.swift
//  donthitgame-ios
//
//  Created by Fikri Can Cankurtaran on 03/11/2017.
//  Copyright © 2017 Solid-ICT. All rights reserved.
//

import UIKit

class LandingPageViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func playButtonTouched(_ sender: Any) {
        
        self.navigationController?.pushViewController(GameSceneViewController(), animated: true)
        
    }
    
}
