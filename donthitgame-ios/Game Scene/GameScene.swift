//
//  GameScene.swift
//  donthitgame-ios
//
//  Created by Fikri Can Cankurtaran on 03/11/2017.
//  Copyright © 2017 Solid-ICT. All rights reserved.
//

//
//  GameScene.swift
//  spritekit-test
//
//  Created by Fikri Can Cankurtaran on 11.09.2017.
//  Copyright © 2017 Fikri Can Cankurtaran. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    weak var gameViewController : GameSceneViewController?
    
    var isFingerOnSpaceShip: Bool = false
    var endYProperty: CGFloat = 0
    
    var bg1: SKSpriteNode!
    var bg2: SKSpriteNode!
    
    var movingSpeed: CGFloat = 5
    
    var timer: Timer?
    
    override func didMove(to view: SKView) {
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(speedUp), userInfo: nil, repeats: true)
        
        addScrollingBG()
        addLinesToFirstNode()
        addLinesToSecondNode()
        addCircle()
        
        physicsWorld.gravity = CGVector.zero
        physicsWorld.contactDelegate = self
        
    }
    
    func addLines(yCoord: CGFloat, background: SKSpriteNode) {
        
        let lineNodeFirst = SKSpriteNode(color: .red, size: CGSize(width: random(min: 100, max: 400), height: 10.0))
        lineNodeFirst.position = CGPoint(x: bg1.frame.midX, y: bg1.size.height - yCoord)
        lineNodeFirst.anchorPoint = CGPoint.zero
        
        let centerPoint = CGPoint(x: lineNodeFirst.size.width / 2 - (lineNodeFirst.size.width * lineNodeFirst.anchorPoint.x), y: lineNodeFirst.size.height / 2 - (lineNodeFirst.size.height * lineNodeFirst.anchorPoint.y))
        lineNodeFirst.physicsBody = SKPhysicsBody(rectangleOf: lineNodeFirst.size, center: centerPoint)
        
        lineNodeFirst.physicsBody?.isDynamic = true
        lineNodeFirst.physicsBody?.categoryBitMask = 2
        lineNodeFirst.physicsBody?.contactTestBitMask = 1
        lineNodeFirst.physicsBody?.collisionBitMask = 0
        
        background.addChild(lineNodeFirst)
        
        let spaceBetweenNodes: CGFloat = 150
        let lineNodeSecondWidth: CGFloat = size.width - lineNodeFirst.size.width - spaceBetweenNodes
        
        let lineNodeSecond = SKSpriteNode(color: .red, size: CGSize(width: lineNodeSecondWidth, height: 10.0))
        lineNodeSecond.anchorPoint = CGPoint.zero
        let lineNodeSecondX = lineNodeFirst.position.x + lineNodeFirst.size.width + spaceBetweenNodes
        lineNodeSecond.position = CGPoint(x: lineNodeSecondX, y: lineNodeFirst.position.y)
        
        let centerPointSecond = CGPoint(x: lineNodeSecond.size.width / 2 - (lineNodeSecond.size.width * lineNodeSecond.anchorPoint.x), y: lineNodeSecond.size.height / 2 - (lineNodeSecond.size.height * lineNodeSecond.anchorPoint.y))
        lineNodeSecond.physicsBody = SKPhysicsBody(rectangleOf: lineNodeSecond.size, center: centerPointSecond)
        
        lineNodeSecond.physicsBody?.isDynamic = true
        lineNodeSecond.physicsBody?.categoryBitMask = 2
        lineNodeSecond.physicsBody?.contactTestBitMask = 1
        lineNodeSecond.physicsBody?.collisionBitMask = 0
        
        background.addChild(lineNodeSecond)
        
    }
    
    func addLinesToFirstNode() {
        
        var amount: CGFloat = 0.0
        for _ in 0...4 {
            self.addLines(yCoord: amount, background: bg1)
            amount = amount + 333.5
        }
        
    }
    
    func addLinesToSecondNode() {
        
        var amount2: CGFloat = 333.5
        for _ in 0...2 {
            self.addLines(yCoord: amount2, background: bg2)
            amount2 = amount2 + 333.5
        }
        
    }
    
    func addCircle() {
        
        let circleNode = SKShapeNode(circleOfRadius: 50.0)
        circleNode.name = "circle"
        let endY = 0 - size.height / 2 + 100
        endYProperty = endY
        circleNode.position = CGPoint(x: 0, y: endY)
        
        circleNode.physicsBody = SKPhysicsBody(circleOfRadius: 50)
        circleNode.strokeColor = .red
        circleNode.fillColor = .red
        circleNode.physicsBody?.isDynamic = true
        circleNode.physicsBody?.categoryBitMask = 1
        circleNode.physicsBody?.contactTestBitMask = 2
        circleNode.physicsBody?.collisionBitMask = 0
        
        self.addChild(circleNode)
        
    }
    
    func addScrollingBG() {
        
        bg1 = SKSpriteNode(color: .clear, size: CGSize(width: frame.size.width, height: frame.size.height))
        bg1.anchorPoint = CGPoint.zero
        bg1.position = CGPoint(x: -(size.width / 2), y: self.frame.maxY)
        addChild(bg1)
        
        bg2 = SKSpriteNode(color: .clear, size: CGSize(width: frame.size.width, height: frame.size.height))
        bg2.anchorPoint = CGPoint.zero
        bg2.position = CGPoint(x: -(size.width / 2), y: self.frame.maxY + bg1.size.height)
        self.addChild(bg2)
        
    }
    
    @objc func speedUp() {
        
        movingSpeed = movingSpeed + 0.1
        
    }
    
    func ballDidCollideWithLine(circle: SKShapeNode, line: SKSpriteNode) {

        self.view?.isPaused = true;
        
        if let scene = GameOverScene(fileNamed: "GameOverScene") {

            scene.gameViewController = self.gameViewController
            scene.scaleMode = .aspectFill
            self.view?.presentScene(scene)
            
        }
        
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        let firstBody: SKPhysicsBody = contact.bodyA
        let secondBody: SKPhysicsBody = contact.bodyB
        
        if let lineNode = firstBody.node as? SKSpriteNode, let
            ball = secondBody.node as? SKShapeNode {
            ballDidCollideWithLine(circle: ball, line: lineNode)
            
        }
        
    }
    
    func touchDown(atPoint pos : CGPoint) {
        
    }
    
    func touchMoved(toPoint pos : CGPoint) {
        
    }
    
    func touchUp(atPoint pos : CGPoint) {
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let firstTouch = touches.first
        guard let firstTouchLocation = firstTouch?.location(in: self) else { return }
        
        let bodies = self.nodes(at: firstTouchLocation)
        
        for body in bodies {
            
            if body.name == "circle" {
                print("CLICKED")
                isFingerOnSpaceShip = true
            }
            
        }
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if isFingerOnSpaceShip {
            
            let touch = touches.first
            
            guard let firstTouchLocation = touch?.location(in: self) else { return }
            guard let previousTouchLocation = touch?.previousLocation(in: self) else { return }
            
            guard let spaceShip = childNode(withName: "circle") as? SKShapeNode else { return }
            
            let spaceShipX = spaceShip.position.x + (firstTouchLocation.x - previousTouchLocation.x)
            
            spaceShip.position = CGPoint(x: spaceShipX, y: spaceShip.position.y)
            
        }
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        isFingerOnSpaceShip = false
        
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        
        bg1.position = CGPoint(x: bg1.position.x, y: bg1.position.y - movingSpeed)
        bg2.position = CGPoint(x: bg2.position.x, y: bg2.position.y - movingSpeed)
        
        if bg1.position.y < -(size.height + bg1.size.height / 2) {
            bg1.position = CGPoint(x: bg1.position.x, y: bg2.position.y + bg2.size.height)
            bg1.removeAllChildren()
            addLinesToFirstNode()
        }
        
        if bg2.position.y < -(size.height + bg2.size.height / 2) {
            bg2.position = CGPoint(x: bg1.position.x, y: bg1.position.y + bg1.size.height)
            bg2.removeAllChildren()
            addLinesToSecondNode()
        }
        
    }
    
    func random() -> CGFloat {
        return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
    }
    
    func random(min: CGFloat, max: CGFloat) -> CGFloat {
        return random() * (max - min) + min
    }
}

