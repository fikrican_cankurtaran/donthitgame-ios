//
//  GameOverScene.swift
//  donthitgame-ios
//
//  Created by Fikri Can Cankurtaran on 03/11/2017.
//  Copyright © 2017 Solid-ICT. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameOverScene: SKScene {
    
    weak var gameViewController : GameSceneViewController?

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let firstTouch = touches.first
        guard let firstTouchLocation = firstTouch?.location(in: self) else { return }
        
        let bodies = self.nodes(at: firstTouchLocation)
        
        for body in bodies {
            
            if body.name == "mainMenu" {
                
                
                UIView.transition(with: (self.gameViewController?.view)!, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    self.gameViewController?.navigationController?.popToRootViewController(animated: false)
                }, completion: nil)
                
            }
            
        }
        
    }

}
